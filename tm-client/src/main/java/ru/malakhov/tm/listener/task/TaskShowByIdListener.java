package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.TaskDto;
import ru.malakhov.tm.endpoint.TaskEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.service.PropertyService;

@Component
public class TaskShowByIdListener extends AbstractTaskShowListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = consoleProvider.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @Nullable final TaskDto task = taskEndpoint.getTaskById(session, id);
        if (task == null) System.out.println("[FAIL]");
        showTask(task);
        System.out.println("[OK]");
    }


    @Override
    public boolean secure() {
        return false;
    }

}
