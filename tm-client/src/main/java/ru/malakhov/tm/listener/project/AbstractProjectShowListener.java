package ru.malakhov.tm.listener.project;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.ProjectDto;
import ru.malakhov.tm.listener.AbstractListener;

public abstract class AbstractProjectShowListener extends AbstractListener {

    protected void showProject(@Nullable final ProjectDto project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

}
