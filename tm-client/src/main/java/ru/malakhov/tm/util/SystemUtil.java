package ru.malakhov.tm.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class SystemUtil {

    public static void showSystemInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory : " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory : " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM : " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryValue = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory from JVM : " + usedMemoryValue);
    }

}