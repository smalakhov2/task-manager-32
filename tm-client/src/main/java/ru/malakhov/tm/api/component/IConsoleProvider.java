package ru.malakhov.tm.api.component;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.exception.system.NumberIncorrectException;

public interface IConsoleProvider {

    @NotNull
    String nextLine();

    @NotNull
    Integer nextNumber() throws NumberIncorrectException;

}