write-host "SERVER STATUS..."

if ( -not ( Test-Path "server.pid" ) ) {
    write-host  "SERVER STOPPED."
    Wait-Event -Timeout 5
    exit 1
}

write-host  "SERVER STARTED."
$response = read-host -Promt "Do you want see server log?(y/n)"

if ( $response -like 'y' ) {
    write-host  "response $response"
    Get-Content log/err.log
}
write-host  "OK"
Read-Host "Press any key to exit"