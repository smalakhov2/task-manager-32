package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDto extends AbstractEntityDto {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name")
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "user_id")
    private String userId;

    public ProjectDto(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDto(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDto(
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final String userId
    ) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof ProjectDto && super.equals(o)) {
            @NotNull final ProjectDto project = (ProjectDto) o;
            return Objects.equals(name, project.name)
                    && Objects.equals(description, project.description)
                    && Objects.equals(userId, project.userId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, userId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}