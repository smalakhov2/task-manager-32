package ru.malakhov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public class AbstractEntityDto implements Serializable {

    @Id
    @NotNull
    @Column(name = "id")
    protected String id = UUID.randomUUID().toString();

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof AbstractEntityDto) {
            @NotNull final AbstractEntityDto abstractEntity = (AbstractEntityDto) o;
            return id.equals(abstractEntity.getId());
        }
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}