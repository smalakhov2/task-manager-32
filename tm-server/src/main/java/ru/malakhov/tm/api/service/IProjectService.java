package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;

import java.util.List;

public interface IProjectService extends IService<ProjectDto, IProjectRepository> {

    void create(
            @Nullable String userId,
            @Nullable String name
    ) throws AbstractException;

    void create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @NotNull
    List<ProjectDto> findAllDto();

    @NotNull
    List<Project> findAllEntity();

    @NotNull
    List<ProjectDto> findAllDtoByUserId(
            @Nullable String userId
    ) throws EmptyUserIdException;

    @NotNull
    List<Project> findAllEntityByUserId(
            @Nullable String userId
    ) throws EmptyUserIdException;

    @Nullable
    ProjectDto findOneDtoById(
            @Nullable String id
    ) throws EmptyIdException;

    @Nullable
    Project findOneEntityById(
            @Nullable String id
    ) throws EmptyIdException;

    @Nullable
    ProjectDto findOneDtoById(
            @Nullable String userId,
            @Nullable String id
    ) throws AbstractException;

    @Nullable
    Project findOneEntityById(
            @Nullable String userId,
            @Nullable String id
    ) throws AbstractException;

    @Nullable
    ProjectDto findOneDtoByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws AbstractException;

    @Nullable
    Project findOneEntityByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws AbstractException;

    @Nullable
    ProjectDto findOneDtoByName(
            @Nullable String userId,
            @Nullable String name
    ) throws AbstractException;

    @Nullable
    Project findOneEntityByName(
            @Nullable String userId,
            @Nullable String name
    ) throws AbstractException;

    void removeAll();

    void removeAllByUserId(@Nullable String userId) throws EmptyUserIdException;

    void removeOneById(@Nullable String id) throws EmptyIdException;

    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws AbstractException;

    void removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws AbstractException;

    void removeOneByName(
            @Nullable String userId,
            @Nullable String name
    ) throws AbstractException;

    void updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    void updateProjectByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

}