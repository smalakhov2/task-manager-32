package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.malakhov.tm.bootstrap.Bootstrap;
import ru.malakhov.tm.config.ApplicationConfig;

public final class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final AbstractApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        context.registerShutdownHook();
        context.getBean(Bootstrap.class).run();
    }

}
