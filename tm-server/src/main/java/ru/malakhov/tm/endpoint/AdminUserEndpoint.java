package ru.malakhov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.malakhov.tm.api.endpoint.IAdminUserEndpoint;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Override
    @WebMethod
    public Result clearAllUser(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        try {
            sessionService.validate(session, Role.ADMIN);
            userService.removeAll();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<UserDto> getAllUserList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        return userService.findAllDto();
    }

    @NotNull
    @Override
    @WebMethod
    public Result lockUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        try {
            sessionService.validate(session, Role.ADMIN);
            sessionService.signOutByLogin(login);
            userService.lockUserByLogin(login);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }


    }

    @NotNull
    @Override
    @WebMethod
    public Result unlockUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        try {
            sessionService.validate(session, Role.ADMIN);
            userService.unlockUserByLogin(login);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        try {
            sessionService.validate(session, Role.ADMIN);
            sessionService.signOutByLogin(login);
            userService.removeOneByLogin(login);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

}