package ru.malakhov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.malakhov.tm.api.endpoint.ISessionEndpoint;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    @WebMethod
    public Result clearAllSession(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            sessionService.validate(session, Role.ADMIN);
            sessionService.removeAll();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<SessionDto> getAllSessionList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        return sessionService.findAllDto();
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDto openSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws AbstractException {
        return sessionService.open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            sessionService.close(session);
            return new Success();
        } catch (final AccessDeniedException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSessionAll(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            sessionService.closeAll(session);
            return new Success();
        } catch (final AccessDeniedException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @WebMethod
    public List<SessionDto> getListSession(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        return sessionService.getListSession(session);
    }

    @Nullable
    @WebMethod
    public UserDto getUser(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        return sessionService.getUser(session);
    }

}