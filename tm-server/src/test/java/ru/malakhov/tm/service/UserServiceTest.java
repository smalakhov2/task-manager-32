package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class UserServiceTest extends AbstractDataTest {

    @NotNull
    private final IUserService userService = context.getBean(UserService.class);

    @NotNull
    private final List<UserDto> allUsers = new ArrayList<>(Arrays.asList(userDto, adminDto));

    public UserServiceTest() throws Exception {
        super();
    }

    @Before
    public void before() {
        userService.persist(allUsers);
    }

    @After
    public void after() {
        userService.removeOne(userDto);
        userService.removeOne(adminDto);
    }

    @Test
    public void testCreate() throws AbstractException {
        userService.create(
                unknownUserDto.getLogin(),
                "1111"
        );
        @Nullable final UserDto user = userService.findOneDtoByLogin(unknownUserDto.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(unknownUserDto.getLogin(), user.getLogin());
        userService.removeOneByLogin(unknownUserDto.getLogin());
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithoutLogin() throws AbstractException {
        userService.create(
                null,
                "1111"
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithoutPassword() throws AbstractException {
        userService.create(
                userDto.getLogin(),
                null
        );
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithEmptyLogin() throws AbstractException {
        userService.create(
                "",
                "1111"
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithEmptyPassword() throws AbstractException {
        userService.create(
                userDto.getLogin(),
                ""
        );
    }

    @Test
    public void testCreateWithEmail() throws AbstractException {
        userService.create(
                unknownUserDto.getLogin(),
                "1111",
                unknownUserDto.getEmail()
        );
        @Nullable final UserDto user = userService.findOneDtoByLogin(unknownUserDto.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(unknownUserDto.getLogin(), user.getLogin());
        Assert.assertEquals(unknownUserDto.getEmail(), user.getEmail());
        userService.removeOneByLogin(unknownUserDto.getLogin());
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithEmailWithoutLogin() throws AbstractException {
        userService.create(
                null,
                "1111",
                userDto.getEmail()
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithEmailWithoutPassword() throws AbstractException {
        userService.create(
                userDto.getLogin(),
                null,
                userDto.getEmail()
        );
    }

    @Test(expected = EmptyEmailException.class)
    public void testCreateWithEmailWithoutEmail() throws AbstractException {
        @Nullable final String email = null;
        userService.create(
                userDto.getLogin(),
                "1111",
                email
        );
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithEmailEmptyLogin() throws AbstractException {
        userService.create(
                "",
                "1111",
                userDto.getEmail()
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithEmailEmptyPassword() throws AbstractException {
        userService.create(
                userDto.getLogin(),
                "",
                userDto.getEmail()
        );
    }

    @Test(expected = EmptyEmailException.class)
    public void testCreateWithEmailEmptyEmail() throws AbstractException {
        userService.create(
                userDto.getLogin(),
                "1111",
                ""
        );
    }

    @Test
    public void testCreateWithRole() throws AbstractException {
        userService.create(
                unknownUserDto.getLogin(),
                "1111",
                Role.ADMIN
        );
        @Nullable User user = userService.findOneEntityByLogin(unknownUserDto.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(unknownUserDto.getLogin(), user.getLogin());
        Assert.assertEquals(user.getRole(), Role.ADMIN);
        userService.removeOneByLogin(unknownUserDto.getLogin());
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithRoleWithoutLogin() throws AbstractException {
        userService.create(
                null,
                "1111",
                Role.USER
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithRoleWithoutPassword() throws AbstractException {
        userService.create(
                userDto.getLogin(),
                null,
                Role.USER
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithRoleWithoutRole() throws AbstractException {
        @Nullable final Role role = null;
        userService.create(
                userDto.getLogin(),
                "",
                role
        );
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithRoleEmptyLogin() throws AbstractException {
        userService.create(
                "",
                "1111",
                Role.USER
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithRoleEmptyPassword() throws AbstractException {
        userService.create(
                userDto.getLogin(),
                "",
                Role.USER
        );
    }

    @Test
    public void testFindAll() {
        @NotNull final List<UserDto> users = userService.findAllDto();
        Assert.assertEquals(4, users.size());
    }

    @Test
    public void testFindAllEntity() {
        @NotNull final List<User> users = userService.findAllEntity();
        Assert.assertEquals(4, users.size());
    }

    @Test
    public void testFindOneDtoById() throws EmptyIdException {
        @Nullable final UserDto user = userService.findOneDtoById(userDto.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(userDto.hashCode(), user.hashCode());
        Assert.assertEquals(userDto, user);

        @Nullable final UserDto nullUser = userService.findOneDtoById(unknownUserDto.getId());
        Assert.assertNull(nullUser);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdWithoutId() throws EmptyIdException {
        userService.findOneDtoById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdWithEmptyId() throws EmptyIdException {
        userService.findOneDtoById("");
    }

    @Test
    public void testFindOneEntityById() throws EmptyIdException {
        @Nullable final User user = userService.findOneEntityById(userDto.getId());
        Assert.assertNotNull(user);

        @Nullable final User nullUser = userService.findOneEntityById(unknownUserDto.getId());
        Assert.assertNull(nullUser);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdWithoutId() throws EmptyIdException {
        userService.findOneEntityById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdWithEmptyId() throws EmptyIdException {
        userService.findOneEntityById("");
    }

    @Test
    public void testFindOneDtoByLogin() throws EmptyIdException, EmptyLoginException {
        @Nullable final UserDto user = userService.findOneDtoByLogin(userDto.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(userDto.hashCode(), user.hashCode());
        Assert.assertEquals(userDto, user);

        @Nullable final UserDto nullUser = userService.findOneDtoByLogin(unknownUserDto.getLogin());
        Assert.assertNull(nullUser);
    }

    @Test(expected = EmptyLoginException.class)
    public void testFindOneDtoByLoginWithoutLogin() throws EmptyLoginException {
        userService.findOneDtoByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testFindOneDtoByLoginWithEmptyLogin() throws EmptyLoginException {
        userService.findOneDtoByLogin("");
    }

    @Test
    public void testFindOneEntityByLogin() throws EmptyLoginException {
        @Nullable final User user = userService.findOneEntityByLogin(userDto.getLogin());
        Assert.assertNotNull(user);

        @Nullable final User nullUser = userService.findOneEntityByLogin(unknownUserDto.getLogin());
        Assert.assertNull(nullUser);
    }

    @Test(expected = EmptyLoginException.class)
    public void testFindOneEntityByLoginWithoutLogin() throws EmptyLoginException {
        userService.findOneEntityByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testFindOneEntityByLoginWithEmptyLogin() throws EmptyLoginException {
        userService.findOneEntityByLogin("");
    }

    @Test
    public void testRemoveAll() throws AbstractException {
        userService.removeAll();
        @NotNull final List<UserDto> tasks = userService.findAllDto();
        Assert.assertTrue(tasks.isEmpty());
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("test", "test");
    }

    @Test
    public void testRemoveOneById() throws EmptyIdException {
        userService.removeOneById(userDto.getId());
        @Nullable final UserDto user = userService.findOneDtoById(userDto.getId());
        Assert.assertNull(user);

        userService.removeOneById(unknownUserDto.getId());
        @NotNull final List<UserDto> tasks = userService.findAllDto();
        Assert.assertEquals(3, tasks.size());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneByIdWithoutId() throws EmptyIdException {
        userService.removeOneById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneByIdWithEmptyId() throws EmptyIdException {
        userService.removeOneById("");
    }

    @Test
    public void testRemoveOneByLogin() throws EmptyLoginException, EmptyIdException {
        userService.removeOneByLogin(userDto.getLogin());
        @Nullable final UserDto user = userService.findOneDtoById(userDto.getId());
        Assert.assertNull(user);

        userService.removeOneByLogin(unknownUserDto.getLogin());
        @NotNull final List<UserDto> tasks = userService.findAllDto();
        Assert.assertEquals(3, tasks.size());
    }

    @Test(expected = EmptyLoginException.class)
    public void testRemoveOneByLoginWithoutLogin() throws EmptyLoginException {
        userService.removeOneByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testRemoveOneByLoginWithEmptyLogin() throws EmptyLoginException {
        userService.removeOneByLogin("");
    }

    @Test
    public void testUpdatePassword() throws AbstractException {
        userService.updatePassword(
                userDto.getId(),
                USER_PASSWORD,
                "test2"
        );
        @Nullable final UserDto user = userService.findOneDtoById(userDto.getId());
        Assert.assertNotNull(user);
        Assert.assertNotEquals(userDto.getPasswordHash(), user.getPasswordHash());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdatePasswordWithoutUserId() throws AbstractException {
        userService.updatePassword(
                null,
                "1111",
                "1112"
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testUpdatePasswordWithoutPassword() throws AbstractException {
        userService.updatePassword(
                userDto.getId(),
                null,
                "1112"
        );
    }

    @Test(expected = EmptyNewPasswordException.class)
    public void testUpdatePasswordWithoutNewPassword() throws AbstractException {
        userService.updatePassword(
                userDto.getId(),
                "1111",
                null
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdatePasswordWithEmptyUserId() throws AbstractException {
        userService.updatePassword(
                "",
                "1111",
                "1112"
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testUpdatePasswordWithEmptyPassword() throws AbstractException {
        userService.updatePassword(
                userDto.getId(),
                "",
                "1112"
        );
    }

    @Test(expected = EmptyNewPasswordException.class)
    public void testUpdatePasswordWithEmptyNewPassword() throws AbstractException {
        userService.updatePassword(
                userDto.getId(),
                "1111",
                ""
        );
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdatePasswordWithUnknownUserId() throws AbstractException {
        userService.updatePassword(
                userDto.getId(),
                "1111",
                "1112"
        );
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdatePasswordWithIncorrectCurrentPassword() throws AbstractException {
        userService.updatePassword(
                userDto.getId(),
                "1113",
                "1112"
        );
    }

    @Test
    public void testUpdateUserInfo() throws AbstractException {
        @NotNull final String newEmail = "yandex@yandex.ru";
        @NotNull final String newFirstName = "Ivan";
        @NotNull final String newLastName = "Ivanov";
        @NotNull final String newMiddleName = "Ivanovich";

        userService.updateUserInfo(
                userDto.getId(),
                newEmail,
                newFirstName,
                newLastName,
                newMiddleName
        );
        @Nullable final UserDto user = userService.findOneDtoById(userDto.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getEmail(), newEmail);
        Assert.assertEquals(user.getFirstName(), newFirstName);
        Assert.assertEquals(user.getLastName(), newLastName);
        Assert.assertEquals(user.getMiddleName(), newMiddleName);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateUserInfoWithouUserId() throws AbstractException {
        userService.updateUserInfo(
                null,
                userDto.getEmail(),
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test(expected = EmptyEmailException.class)
    public void testUpdateUserInfoWithoutEmail() throws AbstractException {
        userService.updateUserInfo(
                userDto.getId(),
                null,
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateUserInfoWithEmptyUserId() throws AbstractException {
        userService.updateUserInfo(
                "",
                userDto.getEmail(),
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test(expected = EmptyEmailException.class)
    public void testUpdateUserInfoWithEmptyEmail() throws AbstractException {
        userService.updateUserInfo(
                userDto.getId(),
                "",
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdateUserInfoWithUnknownUserId() throws AbstractException {
        userService.updateUserInfo(
                unknownUserDto.getId(),
                unknownUserDto.getEmail(),
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test
    public void testLockUserByLogin() throws AbstractException {
        userService.lockUserByLogin(userDto.getLogin());
        @Nullable final UserDto user = userService.findOneDtoById(userDto.getId());
        Assert.assertNotNull(user);
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = EmptyLoginException.class)
    public void testLockUserByLoginWithoutLogin() throws AbstractException {
        userService.lockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testLockUserByLoginWithEmptyLogin() throws AbstractException {
        userService.lockUserByLogin("");
    }

    @Test
    public void testUnlockUserByLogin() throws AbstractException {
        userService.lockUserByLogin(userDto.getLogin());
        userService.unlockUserByLogin(userDto.getLogin());
        @Nullable final UserDto user = userService.findOneDtoByLogin(userDto.getLogin());
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = EmptyLoginException.class)
    public void testUnlockUserByLoginWithoutLogin() throws AbstractException {
        userService.unlockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testUnlockUserByLoginWithEmptyLogin() throws AbstractException {
        userService.unlockUserByLogin("");
    }

}
