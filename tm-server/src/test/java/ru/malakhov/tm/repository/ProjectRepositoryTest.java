package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.DisableOnDebug;
import org.junit.rules.Timeout;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.service.ProjectService;
import ru.malakhov.tm.service.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class ProjectRepositoryTest extends AbstractDataTest {

    @NotNull
    private final IProjectService projectService = context.getBean(ProjectService.class);
    @NotNull
    private final IUserService userService = context.getBean(UserService.class);
    @NotNull
    private final ProjectDto projectOne = new ProjectDto("Project1", "", userDto.getId());
    @NotNull
    private final ProjectDto projectTwo = new ProjectDto("Project2", "", userDto.getId());
    @NotNull
    private final ProjectDto projectThree = new ProjectDto("Project3", "", adminDto.getId());
    @NotNull
    private final ProjectDto projectFour = new ProjectDto("Project4", "", adminDto.getId());
    @NotNull
    private final ProjectDto unknownProject = new ProjectDto("Unknown", "", unknownUserDto.getId());
    @NotNull
    private final List<ProjectDto> userProjects = new ArrayList<>(Arrays.asList(projectOne, projectTwo));
    @NotNull
    private final List<ProjectDto> adminProjects = new ArrayList<>(Arrays.asList(projectThree, projectFour));
    @Rule
    public DisableOnDebug debugTime = new DisableOnDebug(Timeout.seconds(30));

    public ProjectRepositoryTest() throws Exception {
        super();
    }

    @NotNull
    private IProjectRepository getRepository() {
        return projectService.getRepository();
    }

    @Before
    public void before() {
        userService.persist(userDto, adminDto);
        projectService.persist(userProjects);
        projectService.persist(adminProjects);
    }

    @After
    public void after() {
        projectService.removeAll();
        userService.removeOne(userDto);
        userService.removeOne(adminDto);
    }

    @Test
    public void testFindAllDto() {
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final List<ProjectDto> projects = repository.findAllDto();
        Assert.assertEquals(4, projects.size());
    }

    @Test
    public void testFindAllEntity() {
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final List<Project> projects = repository.findAllEntity();
        Assert.assertEquals(4, projects.size());
    }

    @Test
    public void testFindAllDtoByUserId() {
        @NotNull final IProjectRepository repository = getRepository();

        @NotNull final List<ProjectDto> projects = repository.findAllDtoByUserId(userDto.getId());
        Assert.assertEquals(2, projects.size());

        @NotNull final List<ProjectDto> emptyList = repository.findAllDtoByUserId(unknownUserDto.getId());
        Assert.assertEquals(0, emptyList.size());
    }

    @Test
    public void testFindAllEntityByUserId() {
        @NotNull final IProjectRepository repository = getRepository();

        @NotNull final List<Project> projects = repository.findAllEntityByUserId(userDto.getId());
        Assert.assertEquals(2, projects.size());

        @NotNull final List<Project> emptyList = repository.findAllEntityByUserId(unknownUserDto.getId());
        Assert.assertEquals(0, emptyList.size());
    }

    @Test
    public void testFindOneDtoById() {
        @NotNull final IProjectRepository repository = getRepository();

        @Nullable final ProjectDto project = repository.findOneDtoById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project, projectOne);

        @Nullable final ProjectDto unknown = repository.findOneDtoById(unknownProject.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityById() {
        @NotNull final IProjectRepository repository = getRepository();

        @Nullable final Project project = repository.findOneEntityById(projectOne.getId());
        Assert.assertNotNull(project);

        @Nullable final Project unknown = repository.findOneEntityById(unknownProject.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneDtoByIdWithUserId() {
        @NotNull final IProjectRepository repository = getRepository();

        @Nullable final ProjectDto project = repository.findOneDtoById(userDto.getId(), projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project, projectOne);

        @Nullable ProjectDto unknown = repository.findOneDtoById(unknownUserDto.getId(), unknownProject.getId());
        Assert.assertNull(unknown);

        unknown = repository.findOneDtoById(unknownUserDto.getId(), projectOne.getId());
        Assert.assertNull(unknown);

        unknown = repository.findOneDtoById(userDto.getId(), unknownProject.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityByIdWithUserId() {
        @NotNull final IProjectRepository repository = getRepository();

        @Nullable final Project project = repository.findOneEntityById(userDto.getId(), projectOne.getId());
        Assert.assertNotNull(project);

        @Nullable Project unknown = repository.findOneEntityById(unknownUserDto.getId(), unknownProject.getId());
        Assert.assertNull(unknown);

        unknown = repository.findOneEntityById(unknownUserDto.getId(), projectOne.getId());
        Assert.assertNull(unknown);

        unknown = repository.findOneEntityById(userDto.getId(), unknownProject.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneDtoByIndex() {
        @NotNull final IProjectRepository repository = getRepository();

        @Nullable final ProjectDto project = repository.findOneDtoByIndex(userDto.getId(), 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project, projectOne);

        @Nullable ProjectDto unknown = repository.findOneDtoByIndex(unknownUserDto.getId(), 6);
        Assert.assertNull(unknown);

        unknown = repository.findOneDtoByIndex(unknownUserDto.getId(), 0);
        Assert.assertNull(unknown);

        unknown = repository.findOneDtoByIndex(userDto.getId(), 6);
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityByIndex() {
        @NotNull final IProjectRepository repository = getRepository();

        @Nullable final Project project = repository.findOneEntityByIndex(userDto.getId(), 0);
        Assert.assertNotNull(project);

        @Nullable Project unknown = repository.findOneEntityByIndex(unknownUserDto.getId(), 6);
        Assert.assertNull(unknown);

        unknown = repository.findOneEntityByIndex(unknownUserDto.getId(), 0);
        Assert.assertNull(unknown);

        unknown = repository.findOneEntityByIndex(userDto.getId(), 6);
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneDtoByName() {
        @NotNull final IProjectRepository repository = getRepository();

        @Nullable final ProjectDto project = repository.findOneDtoByName(userDto.getId(), projectOne.getName());
        Assert.assertNotNull(project);
        Assert.assertEquals(project, projectOne);

        @Nullable ProjectDto unknown = repository.findOneDtoByName(unknownUserDto.getId(), unknownProject.getName());
        Assert.assertNull(unknown);

        unknown = repository.findOneDtoByName(unknownUserDto.getId(), projectOne.getName());
        Assert.assertNull(unknown);

        unknown = repository.findOneDtoByName(userDto.getId(), unknownProject.getName());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityByName() {
        @NotNull final IProjectRepository repository = getRepository();

        @Nullable final Project project = repository.findOneEntityByName(userDto.getId(), projectOne.getName());
        Assert.assertNotNull(project);

        @Nullable Project unknown = repository.findOneEntityByName(unknownUserDto.getId(), unknownProject.getName());
        Assert.assertNull(unknown);

        unknown = repository.findOneEntityByName(unknownUserDto.getId(), projectOne.getName());
        Assert.assertNull(unknown);

        unknown = repository.findOneEntityByName(userDto.getId(), unknownProject.getName());
        Assert.assertNull(unknown);
    }

    @Test
    public void testRemoveAll() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll();
            repository.commit();
            @NotNull final List<ProjectDto> projects = repository.findAllDto();
            Assert.assertEquals(0, projects.size());
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveAllByUserId() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAllByUserId(unknownUserDto.getId());
            repository.commit();
            @NotNull List<ProjectDto> projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeAllByUserId(userDto.getId());
            repository.commit();
            projects = repository.findAllDto();
            Assert.assertEquals(2, projects.size());
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(unknownProject.getId());
            repository.commit();
            @NotNull List<ProjectDto> projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneById(projectOne.getId());
            repository.commit();
            @Nullable final ProjectDto project = repository.findOneDtoById(projectOne.getId());
            Assert.assertNull(project);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneByIdWithUserId() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(unknownUserDto.getId(), unknownProject.getId());
            repository.commit();
            @NotNull List<ProjectDto> projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneById(userDto.getId(), unknownProject.getId());
            repository.commit();
            projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneById(unknownUserDto.getId(), projectOne.getId());
            repository.commit();
            projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneById(userDto.getId(), projectOne.getId());
            repository.commit();
            @Nullable final ProjectDto project = repository.findOneDtoById(projectOne.getId());
            Assert.assertNull(project);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final IProjectRepository repository = getRepository();

        try {
            repository.begin();
            repository.removeOneByIndex(unknownUserDto.getId(), 6);
            repository.commit();
            @NotNull List<ProjectDto> projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneByIndex(userDto.getId(), 6);
            repository.commit();
            projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneByIndex(unknownUserDto.getId(), 0);
            repository.commit();
            projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneByIndex(userDto.getId(), 0);
            repository.commit();
            @Nullable final ProjectDto project = repository.findOneDtoById(projectOne.getId());
            Assert.assertNull(project);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final IProjectRepository repository = getRepository();

        try {
            repository.begin();
            repository.removeOneByName(unknownUserDto.getId(), unknownProject.getName());
            repository.commit();
            @NotNull List<ProjectDto> projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneByName(userDto.getId(), unknownProject.getName());
            repository.commit();
            projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneByName(unknownUserDto.getId(), projectOne.getName());
            repository.commit();
            projects = repository.findAllDto();
            Assert.assertEquals(4, projects.size());

            repository.begin();
            repository.removeOneByName(userDto.getId(), projectOne.getName());
            repository.commit();
            @Nullable final ProjectDto project = repository.findOneDtoById(projectOne.getId());
            Assert.assertNull(project);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
